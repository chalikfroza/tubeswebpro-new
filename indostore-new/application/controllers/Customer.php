<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function index()
	{
		$data['judul'] = 'indostore';
		$this->load->view('template/header2',$data);
		$this->load->view('customer/account');
		$this->load->view('template/footer');		
	}

	public function regis()
	{
		$data['judul'] = 'indostore';
		$this->load->view('template/header2',$data);
		$this->load->view('customer/regis');
		$this->load->view('template/footer');	
	}

	public function tambah_alamat()
	{
		$data['judul'] = 'indostore';
		$this->load->view('template/header2',$data);
		$this->load->view('customer/tambah_alamat');
		$this->load->view('template/footer');	
	}	

}

/* End of file customer.php */
/* Location: ./application/controllers/customer.php */