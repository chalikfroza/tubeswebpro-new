<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccustomer extends CI_Controller {

	function __construct(){
	    parent::__construct();
	    $this->load->model('LoginModel');
	    $this->load->helper('date');
	}

	public function index()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header2', $data);
		$this->load->view('customer/account');
		$this->load->view('template/footer');
	}


	public function profile()
	{
		$data['judul'] = 'indoStore';
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$data['alamat'] = $this->db->get_where('alamat', ['id_user' =>  $this->session->userdata('idUser')])->row(1);
		$data['jalamat'] = $this->db->join('user', 'alamat.id_user = user.idUser')->get_where('alamat', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$this->load->view('template/header2', $data);
		$this->load->view('customer/profile', $data);
		$this->load->view('template/footer');
	}

	public function informasi_akun($id)
	{
		$data['judul'] = 'indoStore';
		/*get buat placeholder*/
		
		$data['pengguna'] = $this->LoginModel->getidUser($id);
		$this->form_validation->set_rules('namadepan', 'Namadepan', 'required');
		$this->form_validation->set_rules('namabelakang', 'Namabelakang', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('template/header2', $data);	
			$this->load->view('customer/informasi_akun',$data);
			$this->load->view('template/footer');
		}else{
			$this->LoginModel->ubahDataAkun();
			$this->session->set_flashdata('akun','Berhasil Diubah');
			redirect('Ccustomer/profile');
		}
	}

	public function buku_alamat()
	{
		$data['judul'] = 'indoStore';
		$data['alamat'] = $this->db->join('user', 'alamat.id_user = user.idUser')->get_where('alamat', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/buku_alamat',$data);
		$this->load->view('template/footer');
	}

	public function edit_alamat($id)
	{
		$data['judul'] = 'indoStore';	
		$data['alamat'] = $this->LoginModel->getAlamatiduser($id);

		$this->form_validation->set_rules('alamatJalan', 'AlamatJalan', 'required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
		$this->form_validation->set_rules('kota', 'Kota', 'required');
		$this->form_validation->set_rules('kodepos', 'Kodepos', 'required');
		$this->form_validation->set_rules('noTelp', 'NoTelp', 'required');

		if($this->form_validation->run()==FALSE){
			$this->load->view('template/header2',$data);
			$this->load->view('customer/edit_alamat',$data);
			$this->load->view('template/footer');
		}else{
			$this->LoginModel->ubahDataAlamat();
			$this->session->set_flashdata('alamat','Berhasil Diubah');
			redirect('Ccustomer/buku_alamat');
		}
	}

	public function editnama($id){
		$data['pengguna'] = $this->LoginModel->getidUser($id);
		$id = $pengguna->idUser;
		$this->LoginModel->ubahnama($id);
		redirect('Ccustomer/informasi_akun');
	}

	public function hapus_alamat($id)
	{
		//call method hapusDataMahasiswa with parameter id from mahasiswa_model
		//use flashdata to show alert "dihapus"
		//back to controller mahasiswa
		$this->LoginModel->hapusalamat($id);
		$this->session->set_flashdata('alamat', 'berhasil Dihapus');
		redirect('Ccustomer/buku_alamat');
	}

	public function tambah_alamat()
	{
		$data = $this->input->post();
		$data['id_user'] = $this->session->userdata('idUser');
		$query = $this->db->insert('alamat', $data);
		if ($query) {
			$this->session->set_flashdata('alamat', 'berhasil Ditambahkan');
			redirect('Ccustomer/buku_alamat');
		}
	}

	public function pesanan()
	{
		$data['judul'] = 'indoStore';
		$this->db->select('*');
		$this->db->from('pesanan');
		$this->db->join('alamat', 'pesanan.alamatid = alamat.id_alamat');
		$this->db->join('produk','pesanan.produkid = produk.id_produk');
		$this->db->join('user','pesanan.userid = user.idUser');
		$this->db->where('pesanan.userid',$this->session->userdata('idUser'));
		$pes = $this->db->get();
		$data['pesanan'] = $pes->result();
		$data['id_user'] = $this->session->userdata('idUser');
		$this->load->view('template/header2', $data);	
		$this->load->view('Customer/pesanan',$data);
		$this->load->view('template/footer');
	}

	public function newslatter()
	{
		$data['judul'] = 'indoStore';
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$this->load->view('template/header2', $data);	
		$this->load->view('Customer/newslatter',$data);
		$this->load->view('template/footer');
	}	

	public function ulasan()
	{
		$data['judul'] = 'indoStore';
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$this->load->view('template/header2', $data);	
		$this->load->view('Customer/ulasan',$data);
		$this->load->view('template/footer');
	}	

	public function ulasan_detil()
	{
		$data['judul'] = 'indoStore';
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$this->load->view('template/header2', $data);	
		$this->load->view('Customer/ulasan_detil',$data);
		$this->load->view('template/footer');
	}

	public function wishlist()
	{
		$data['judul'] = 'indoStore';
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$this->load->view('template/header2', $data);	
		$this->load->view('Customer/wishlist',$data);
		$this->load->view('template/footer');
	}

	public function regis()
	{
		$data['judul'] = 'indoStore';
		$namad = $this->input->post('namadepan');
		$namab = $this->input->post('namabelakang');
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		$repass = $this->input->post('kpassword');
		/*if(isSama($pass, $repass)){*/
			$data1 = array(
				'namadepan' => $namad,
				'namabelakang' => $namab,
				'email' => $email,
				'password' =>$pass
			); 
			
			$table = 'user';
			$apaja = $this->LoginModel->register_user($table, $data1);
			if($apaja){
				$this->session->set_flashdata('flash', 'Registrasi Berhasil');
			}


		redirect('Ccustomer');
	}
	public function isSama($pass, $kpass)
	{
		if($pass == $kpass){
			return true;
		}
		else{
			return false;
		}
	}

	public function login()
	{
	 // 	$email = $this->input->post('email');
		// $password = $this->input->post('password');
   
		// $user = $this->LoginModel->login_user($email, $password);
		// $admin = $this->LoginModel->login_admin($email, $password);
  //  		$sess_data = array(/*inisialisasi session*/
		// 	'logged_in' => '',
		// 	'email' => ''
		// );	
		// if ($user) {/*jika usser yang melakukan login*/
		// 	$login = $this->db->get_where('user', array('email' => $email, 'password' => $password));

		//   	$this->session->set_userdata($login);
		//   	redirect('Ccustomer/profile');
		// }
		// else if($admin){jika admin yang melakukan login
	 //    	$sess_data['logged_in']= 'Admin';
		//    	$sess_data['email'] = $admin->email;
		//    $this->session->set_userdata('logged_in', $sess_data['logged_in']);
		//    $this->session->set_userdata('email', $sess_data['email']);
		//    redirect('Ccustomer/admin');
		//  } 
		//  else {

		// 	echo "<script>alert('Gagal login: Cek email, password!');</script>";
		// 	$data['judul'] = 'indoStore';
		//  	$this->load->view('template/header', $data);
		//  	$this->load->view('customer/account');
		//  	$this->load->view('template/footer'); 
		// }
		$data = $this->input->post();
		$login = $this->db->get_where('user', ['email' => $data['email'], 'password' => $data['password']]);
		if ($login->num_rows() == 0) {
			$this->session->set_flashdata('error', 'password dan email salah');
			redirect('Ccustomer');
			return;
		}

		if ($login->row(1)->role == 1) {
			$this->session->set_userdata($login->row_array(1));
			$this->session->set_flashdata('success', 'Selamat datang ');
			redirect('/Admin');
			return;
		}

		$this->session->set_userdata($login->row_array(1));
		$this->session->set_flashdata('success', 'Selamat datang ');
		redirect('Ccustomer/profile');
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect('Ccustomer');
	}
  
}
?>