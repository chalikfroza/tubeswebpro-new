<div class="container mt-3"> 
		<div class="row"> 
				<div class="col">
                                    <h5>Informasi Pribadi</h5>
                                    <p class="border-bottom mb-4 mt-2"></p>
                                </div>
                            </div>
                            <form action="" enctype="multipart/form-data" method="POST">
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Kode Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="kode" name="kode" required>
                                </div>
                            </div>
                            <div class="row mb-3 text-left">
                                <div class="col"><small>Kategori :</small></div>
                                <div class="col"><small>mountaineering</small></div>
                                <div class="col"><small>1989</small></div>
                                <div class="col"><small>riding</small></div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Kategori Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="kategori" name="kategori" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Nama Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="namabarang" name="namabarang" required>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Deskripsi</label>
                                </div>
                                <div class="col">
                                    <textarea type="text" class="form-control mb-4" id="deskripsi" name="deskripsi" required></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Gambar</label>
                                </div>
                                <div class="col">
                                    <input type="file" id="gambar" name="gambar" >
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Harga</label>
                                </div>
                                <div class="col">
                                    <input type="number" min="0" max="10000000" class="form-control mb-4" id="harga" name="harga" value="0" required>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-5">
                                    <button type="submit" name="masuk" class="btn btn-primary btn-block">Submit</button>
                                </div>
                                <div class="col-6"></div>
                                <a href="<?= base_url(); ?>Admin"><small>kembali</small></a>
                            </div>
                            </form>
                        </div>
		</div>			
</div>	