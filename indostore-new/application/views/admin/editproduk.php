<div class="container mt-3"> 
		<div class="row"> 
				<div class="col">
                                    <h5>Informasi Pribadi</h5>
                                    <p class="border-bottom mb-4 mt-2"></p>
                                </div>
                            </div>
                            <form action="" enctype="multipart/form-data" method="POST">
                                <input type="hidden" id="id" name="id" value="<?=$produk['id_produk']?>">
                                <input type="hidden" id="gambar_lama" name="gambar_lama" value="<?=$produk['gambar']?>">
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Kode Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="kode" name="kode" value="<?= $produk['kode'] ?>" required>
                                </div>
                            </div>
                            <div class="row mb-3 text-left">
                                <div class="col"><small>Kategori :</small></div>
                                <div class="col"><small>mountaineering</small></div>
                                <div class="col"><small>1989</small></div>
                                <div class="col"><small>riding</small></div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Kategori Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="kategori" name="kategori" value="<?= $produk['kategori'] ?>" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Nama Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="namabarang" name="namabarang" value="<?= $produk['namabarang'] ?>"required>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Deskripsi</label>
                                </div>
                                <div class="col">
                                    <textarea type="text" class="form-control mb-4" id="deskripsi" name="deskripsi"" required><?=$produk['deskripsi']?></textarea>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Gambar</label>
                                </div>
                                <div class="row ml-1">
                                    <div class="col">
                                        <img src="<?= base_url() ?>/assets/image/<?=$produk['gambar'] ?>" width="100px">
                                            <div class="row">   
                                                <div class="col-2">
                                                    <input type="file" name="gambar" id="gambar" >
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-3">
                                    <label for="nama">Harga</label>
                                </div>
                                <div class="col">
                                    <input type="number" min="0" max="10000000" class="form-control mb-4" id="harga" name="harga" value="<?= $produk['harga'] ?>" required>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-5">
                                    <button type="submit" name="masuk" class="btn btn-primary btn-block">Edit Produk</button>
                                </div>
                                <div class="col-6"></div>
                                <a href="<?= base_url(); ?>Admin"><small>kembali</small></a>
                            </div>
                            </form>
                        </div>
		</div>			
</div>	