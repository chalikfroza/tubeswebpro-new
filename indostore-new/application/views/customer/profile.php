<div class="container">
	<div class="row">
		<div class="col">
			<?php if ($this->session->flashdata('akun')): ?>    
		        <div class="row">
		            <div class="col-md-8 mx-auto text-center">
		                <div class="alert alert-success alert-dismissible fade show" role="alert">
		                    Akun <strong> <?= $this->session->flashdata('akun');  ?></strong>
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                    </button>
		                </div>
		            </div>
		        </div>
		    <?php endif ?>
			<?php if ($this->session->flashdata('success')): ?>    
		        <div class="row">
		            <div class="col-md-8 mx-auto text-center">
		                <div class="alert alert-success alert-dismissible fade show" role="alert">
		                     <strong> <?= $this->session->flashdata('success').' '.$pengguna->namadepan  ?></strong>
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                    </button>
		                </div>
		            </div>
		        </div>
		    <?php endif ?>
		</div>
	</div>
</div>

<div class="container">
<div class="row">
	<div class="col-3">
	<div class="list-group">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action active">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun/<?= $pengguna->idUser?>" type="button" class="list-group-item list-group-item-action">Informasi Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action">Buku Alamat</a>
			<a href="<?php echo base_url()?>Ccustomer/pesanan" class="list-group-item list-group-item-action">Pesanan Saya</a>
			<a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>					
		</div>
	</div>
	<div class="col mt-3">
		<h2>Dashboard</h2>
		<div class="bawah bg-primary"></div>
		<div class="row mt-3 ">
			<div class="col">
				<h4>Informasi Akun</h4>
			</div>
		</div>
		
		<div class="row">
			<div class="col-4">
				<p class="border-bottom">Informasi Kontak
					<a href="<?php echo base_url() ?>Ccustomer/informasi_akun/<?= $pengguna->idUser?>" class="ml-5"> <!-- //halaman informasi kontak// -->
						<img src="<?php echo base_url();?>assets/pensil.png" style="width: 14px; height: 14px">
					</a>
				</p>

					
				
				<div class="container">

					<div class="row">
			    		<small><?php echo $pengguna->namadepan.' '. $pengguna->namabelakang;?></small> <!-- ubah email sesuai dengan db -->
					</div>
					<div class="row">
			    		<small><?php if ($pengguna->role == 2) {
			    			echo "pengguna";
			    		} ?></small> <!-- ubah email sesuai dengan db -->
					</div>
					<div class="row">
			    		<small><?php echo $pengguna->email ?></small> <!-- ubah email sesuai dengan db -->
					</div>
				</div>
				
			</div>

			<div class="col-3"></div>
			<div class="col-4">
				<p class="border-bottom">Newslatter
					<a href="<?php echo base_url() ?>Ccustomer/newslatter" class="ml-5"> <!-- //halaman informasi kontak// -->
						<img src="<?php echo base_url();?>assets/pensil.png" style="width: 14px; height: 14px">
					</a>
				</p>
				<small class="mt-2">
				Anda sudah berlangganan Newsletter.."
				</small> <!-- ubah nama sesuai dengan db -->
			</div>
		</div>		

		<div class="row mt-3">
			<div class="col-3 mt-5">
				<h4>Buku Alamat </h4>
			</div>
			<div class="col mt-5">
				<small><a href="<?php echo base_url() ?>Ccustomer/buku_alamat">manage addressed</a></small>
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<p class="border-bottom">Alamat Pembayaran
					<a href="<?php echo base_url() ?>Ccustomer/edit_alamat/<?= $pengguna->idUser?>" class="ml-4"> <!-- //halaman informasi kontak// -->
						<img src="<?php echo base_url();?>assets/pensil.png" style="width: 14px; height: 14px">
					</a>
				</p>
				<div class="container">
					<?php if ($alamat != null ): ?>
						<div class="row">
					    	<small><?php echo $pengguna->namadepan ?> <?php echo $pengguna->namabelakang ?></small>
						</div>
						<div class="row">
					    	<small><?php echo $alamat->alamatJalan ?></small>
						</div>
						<div class="row">
					    	<small><?php echo $alamat->kota?> <?php echo $alamat->provinsi?> <?php echo $alamat->kodepos ?></small>
						</div>
						<div class="row">
					    	<small><?php echo $alamat->noTelp ?></small>
						</div>
					<?php endif ?>

					<?php if ($alamat == null): ?>
						<div class="row">
							<small>Silahkan Tambahkan Alamat</small>
						</div>
					<?php endif ?>
				</div>

			</div>

			<div class="col-3"></div>
			<div class="col-4">
				<p class="border-bottom">Alamat Pengirim
					<a href="<?php echo base_url() ?>Ccustomer/edit_alamat" class="ml-5"> <!-- //halaman informasi kontak// -->
						<img src="<?php echo base_url();?>assets/pensil.png" style="width: 14px; height: 14px">
					</a>
				</p>
				<small class="mt-2"></small>
			</div>
		</div>	
		
	</div>
</div>
</div>
</div>