<div class="container">
<div class="row">
	<div class="col-3">
		<div class="list-group">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun/<?= $pengguna['idUser']?>" type="button" class="list-group-item list-group-item-action active ">Informasi Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action">Buku Alamat</a>
			<a href="<?php echo base_url()?>Ccustomer/pesanan" type="button" class="list-group-item list-group-item-action">Pesanan Saya</a>
			<a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>					
		</div>
	</div>
	<div class="col mt-3">
		<h2>Edit informasi Akun</h2>
		<div class="bawah bg-primary"></div>
		<div class="row mt-3 ">
			<div class="col-6">
				<h4 class="border-bottom">Informasi Akun</h4>
			</div>
		</div>
		<form action="" method="POST">
				<div class="row mt-4">
					<div class="col">
						<p>Nama Depan</p>
						<input type="hidden" id="id" name="id" value="<?php echo $pengguna['idUser']; ?>">
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control mb-4 " id="namadepan" name="namadepan" value="<?php echo $pengguna['namadepan'] ?>">
					</div>
				</div>
				<div class="row">
						<p class="col">Nama Belakang</p>
				</div>
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control mb-4" id="namabelakang" name="namabelakang" value="<?php echo $pengguna['namabelakang'] ?>">
					</div>
				</div>
				<div class="row ">
					<div class="col">
						<p>Email</p>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<input type="text" class="form-control mb-4" id="email" name="email" value="<?php echo $pengguna['email']; ?>">
					</div>
				</div>
				<div class="row">
					<div class="col">
						<p>Password Saat ini</p>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<input type="password" class="form-control mb-4" id="password" name="password" value="<?php echo $pengguna['password']; ?>">
					</div>
				</div>
				
				<div class="row">
					<div class="col-3">
						<button type="submit" name="edit3" id="edit3" class="btn btn-primary btn-block" onclick="return confirm('Yakin ingin diubah ?')">Ubah Data</button>
					</div>
					<div class="col ml-5 text-right">
						<small><a href="<?php echo base_url()?>Ccustomer/profile">kembali</a></small>
					</div>
				</div>
		</form>
	</div>
</div>
</div>
</div>